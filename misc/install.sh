#!/bin/sh

OPENSHIFT_RUNTIME_DIR=$OPENSHIFT_HOMEDIR/app-root/runtime
OPENSHIFT_REPO_DIR=$OPENSHIFT_HOMEDIR/app-root/runtime/repo

# PHP https://secure.php.net/downloads.php
VERSION_PHP=5.6.31

# Apache http://www.gtlib.gatech.edu/pub/apache/httpd/
VERSION_APACHE=2.4.27
# APR http://artfiles.org/apache.org/apr/
VERSION_APR=1.6.2
VERSION_APR_UTIL=1.6.0

# PCRE ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre
VERSION_PCRE=8.41

# XDebug http://xdebug.org/files/
VERSION_XDEBUG=2.5.4

# ZLib http://zlib.net/
VERSION_ZLIB=1.2.11

echo "Prepare directories"

cd $OPENSHIFT_HOMEDIR/app-root/repo
mkdir www
chmod -R755 www

cd $OPENSHIFT_HOMEDIR/app-root/runtime
mkdir srv
mkdir srv/pcre
mkdir srv/httpd
mkdir srv/php
mkdir tmp

cd tmp/

echo "Install pcre"
wget ftp://ftp.csx.cam.ac.uk/pub/software/programming/pcre/pcre-$VERSION_PCRE.tar.gz
tar -zxf pcre-$VERSION_PCRE.tar.gz
cd pcre-$VERSION_PCRE
./configure \
--prefix=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/pcre
make && make install
cd ..

echo "Install Apache httpd"
wget http://www-us.apache.org/dist//httpd/httpd-$VERSION_APACHE.tar.gz
tar -zxf httpd-$VERSION_APACHE.tar.gz
wget http://artfiles.org/apache.org/apr/apr-$VERSION_APR.tar.gz
tar -zxf apr-$VERSION_APR.tar.gz
mv apr-$VERSION_APR httpd-$VERSION_APACHE/srclib/apr
wget http://artfiles.org/apache.org/apr/apr-util-$VERSION_APR_UTIL.tar.gz
tar -zxf apr-util-$VERSION_APR_UTIL.tar.gz
mv apr-util-$VERSION_APR_UTIL httpd-$VERSION_APACHE/srclib/apr-util
cd httpd-$VERSION_APACHE
./configure \
--prefix=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/httpd \
--with-included-apr \
--with-pcre=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/pcre \
--enable-so \
--enable-auth-digest \
--enable-rewrite \
--enable-setenvif \
--enable-mime \
--enable-deflate \
--enable-headers
make && make install
cd ..

#echo "INSTALL ICU"
#wget http://download.icu-project.org/files/icu4c/50.1/icu4c-50_1-src.tgz
#tar -zxf icu4c-50_1-src.tgz
#cd icu/source/
#chmod +x runConfigureICU configure install-sh
#./configure \
#--prefix=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/icu/
#make && make install
#cd ../..

echo "Install zlib"
wget http://zlib.net/zlib-$VERSION_ZLIB.tar.gz
tar -zxf zlib-$VERSION_ZLIB.tar.gz
cd zlib-$VERSION_ZLIB
./configure \
--prefix=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/zlib/
make && make install
cd ..

echo "INSTALL PHP"
wget http://de2.php.net/get/php-$VERSION_PHP.tar.gz/from/this/mirror -O php-$VERSION_PHP.tar.gz
tar -zxf php-$VERSION_PHP.tar.gz
cd php-$VERSION_PHP
./configure \
--with-libdir=lib64 \
--prefix=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/php/ \
--with-config-file-path=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/php/etc/apache2 \
--with-config-file-scan-dir=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/php/conf.d/ \
--with-apxs2=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/httpd/bin/apxs \
--with-zlib=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/zlib \
--with-jpeg-dir \
--with-png-dir \
--with-freetype-dir \
--with-layout=PHP \
--with-curl \
--with-pear \
--with-gd \
--with-zlib \
--with-mhash \
--with-mysql \apache2 -v
--with-pgsql \
--with-mysqli \
--with-pdo-mysql \
--with-pdo-pgsql \
--with-openssl \
--with-xmlrpc \
--with-xsl \
--with-bz2 \
--with-gettext \
--with-readline \
--with-kerberos \
--with-pcre-regex \
--disable-debug \
--enable-cli \
--enable-inline-optimization \
--enable-exif \
--enable-wddx \
--enable-zip \
--enable-bcmath \
--enable-calendar \
--enable-ftp \
--enable-mbstring \
--enable-soap \
--enable-sockets \
--enable-shmop \
--enable-dba \
--enable-sysvsem \
--enable-sysvshm \
--enable-sysvmsg \
--enable-opcache \
--enable-intl
#--enable-intl \
#--with-icu-dir=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/icu \

make && make install
mkdir $OPENSHIFT_HOMEDIR/app-root/runtime/srv/php/etc/apache2
cd ..

#echo "Install APC"
#wget http://pecl.php.net/get/APC-3.1.13.tgz
#tar -zxf APC-3.1.13.tgz
#cd APC-3.1.13
#$OPENSHIFT_HOMEDIR/app-root/runtime/srv/php/bin/phpize
#./configure \
#--with-php-config=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/php/bin/php-config \
#--enable-apc \
#--enable-apc-debug=no
#make && make install
#cd ..

echo "Install xdebug"
wget http://xdebug.org/files/xdebug-$VERSION_XDEBUG.tgz
tar -zxf xdebug-$VERSION_XDEBUG.tgz
cd xdebug-$VERSION_XDEBUG
$OPENSHIFT_HOMEDIR/app-root/runtime/srv/php/bin/phpize
./configure \
--with-php-config=$OPENSHIFT_HOMEDIR/app-root/runtime/srv/php/bin/php-config
make && cp modules/xdebug.so $OPENSHIFT_HOMEDIR/app-root/runtime/srv/php/lib/php/extensions
cd ..

echo "Cleanup"
rm -r $OPENSHIFT_HOMEDIR/app-root/runtime/tmp/*.tar.gz
rm -r $OPENSHIFT_HOMEDIR/app-root/runtime/tmp/*.tgz

echo "COPY TEMPLATES"
cp $OPENSHIFT_REPO_DIR/misc/templates/bash_profile.tpl $OPENSHIFT_HOMEDIR/app-root/data/.bash_profile
python $OPENSHIFT_REPO_DIR/misc/parse_templates.py

echo "START APACHE"
$OPENSHIFT_HOMEDIR/app-root/runtime/srv/httpd/bin/apachectl start

cd $OPENSHIFT_HOMEDIR/app-root/repo/www
PATH=${OPENSHIFT_HOMEDIR}/app-root/runtime/srv/php/bin

echo "*****************************"
echo "***  F I N I S H E D !!   ***"
echo "*****************************"
